package com.example.tugas2_fix;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button=findViewById(R.id.login);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText inputUsername = findViewById(R.id.inputUsername);
                EditText inputPassword = findViewById(R.id.inputPassword);

                String username = inputUsername.getText().toString();
                String password = inputPassword.getText().toString();

                if (isValidLogin(username, password)) {
                    Intent intent = new Intent(MainActivity.this, isi.class);
                    intent.putExtra("username", username);
                    intent.putExtra("password", password);
                    startActivity(intent);
                } else {
                    // Memunculkan tampilan gagal login
                    Toast.makeText(MainActivity.this, "Invalid username or password", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    private boolean isValidLogin(String username, String password) {
        // Username dan Password yang Valid
        return (username.equals("Budi Doremi") && password.equals("123456789")) ||
                (username.equals("Ramadhan") && password.equals("456"));
    }
}
