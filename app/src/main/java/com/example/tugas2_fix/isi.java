package com.example.tugas2_fix;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class isi extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi);

        TextView textViewUsername = findViewById(R.id.hasilInputUsername);
        TextView textViewPassword = findViewById(R.id.hasilInputPassword);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String username = extras.getString("username");
            String password = extras.getString("password");

            textViewUsername.setText("Username: " + username);
            textViewPassword.setText("Password: " + password);
        }
    }
}